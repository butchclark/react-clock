import React, {Component} from 'react';
import PropTypes from 'prop-types';

function sleep(ms){
    return new Promise(resolve=> setTimeout(resolve,ms))
}

class Clock extends Component {
    constructor(props){
        super(props)
        this.state = {time: new Date().toLocaleTimeString()}
        this.bumpTime()
    }

    async bumpTime(){
        await sleep(1000)
        this.setState({time: new Date().toLocaleTimeString()})
        this.bumpTime()
    }

    render() {
        return (
            <div className="app-title app-intro clock-face">
                {this.state.time}
            </div>
        );
    }
}

Clock.propTypes = {};

export default Clock;
